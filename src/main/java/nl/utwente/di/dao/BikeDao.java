package nl.utwente.di.dao;

import nl.utwente.di.model.Bike;

import java.util.HashMap;
import java.util.Map;

public enum BikeDao {
	instance;

	private Map<Integer, Bike> contentProvider = new HashMap<Integer, Bike>();

	private BikeDao() {}

	public Map<Integer, Bike> getModel() {
		return contentProvider;
	}

}