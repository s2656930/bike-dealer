package nl.utwente.di.resources;

import jakarta.ws.rs.*;
import jakarta.ws.rs.core.*;
import jakarta.xml.bind.JAXBElement;
import nl.utwente.di.dao.BikeDao;
import nl.utwente.di.model.Bike;


public class BikeResource {
  @Context
  UriInfo uriInfo;
  @Context
  Request request;
  int id;
  public BikeResource(UriInfo uriInfo, Request request, int id) {
    this.uriInfo = uriInfo;
    this.request = request;
    this.id = id;
  }
  

  @GET
  @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_XML})
  public Bike getBike() {
    Bike bike = BikeDao.instance.getModel().get(id);
    if(bike == null) {
      throw new RuntimeException("Get: Bike with " + id + " not found");
    }
    return bike;
  }

  @PUT
  @Consumes(MediaType.APPLICATION_XML)
  @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_XML})
  public Bike putTodo(JAXBElement<Bike> bike) {
    Bike b = bike.getValue();
    putAndGetResponse(b);
    return b;
  }
  
  @DELETE
  public void deleteBike() {
    Bike b = BikeDao.instance.getModel().remove(id);
    if(b == null) {
      throw new RuntimeException("Delete: Bike with " + id + " not found");
    }
  }

  @Path("order")
  @POST
  @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
  @Produces(MediaType.TEXT_PLAIN)
  public String orderBike(@FormParam("ownerName") String ownerName) {
    Bike b = BikeDao.instance.getModel().get(id);
    if(b == null) {
      return "fail";
    }
    b.setOwnerName(ownerName);
    return "success";
  }
  
  private Response putAndGetResponse(Bike bike) {
    Response res;
    if(BikeDao.instance.getModel().containsKey(bike.getId())) {
      res = Response.noContent().build();
    } else {
      res = Response.created(uriInfo.getAbsolutePath()).build();
    }
    BikeDao.instance.getModel().put(bike.getId(), bike);
    return res;
  }
  

} 