package nl.utwente.di.resources;


import jakarta.servlet.http.HttpServletResponse;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Request;
import jakarta.ws.rs.core.UriInfo;
import nl.utwente.di.dao.BikeDao;
import nl.utwente.di.model.Bike;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

// Will map the resource to the URL bikes
@Path("/bikes")
public class BikesResource {

  // Allows to insert contextual objects into the class, 
  // e.g. ServletContext, Request, Response, UriInfo
  @Context
  UriInfo uriInfo;
  @Context
  Request request;
  
  @POST
  @Produces(MediaType.TEXT_HTML)
  @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
  public void createBike(@FormParam("color") String color,
                         @FormParam("gender") String gender,
                         @Context HttpServletResponse servletResponse) throws IOException
  {
    Bike bike = new Bike(color, gender);
    BikeDao.instance.getModel().put(bike.getId(), bike);

    servletResponse.sendRedirect("../index.html");
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public List<Bike> getBikes(@QueryParam("color") String color,
                             @QueryParam("gender") String gender)
  {
    if (color == null) color = "";
    if (gender == null) gender = "";
    List<Bike> bikes = new ArrayList<>();
    if (color.isEmpty() && gender.isEmpty()) {
      bikes.addAll(BikeDao.instance.getModel().values());
      return bikes;
    }
    for(Map.Entry<Integer, Bike> bike : BikeDao.instance.getModel().entrySet()) {
      if (!gender.isEmpty()) {
        if (!color.isEmpty()) {
          if (bike.getValue().getColor().equals(color) && bike.getValue().getGender().equals(gender)) {
            bikes.add(bike.getValue());
          }
        } else {
          if (bike.getValue().getGender().equals(gender)) {
            bikes.add(bike.getValue());
          }
        }
      } else {
        if (bike.getValue().getColor().equals(color)) {
          bikes.add(bike.getValue());
        }
      }
    }
    return bikes;
  }

  @Path("{bikeId}")
  public BikeResource getBike(@PathParam("bikeId") int id)
  {
    return new BikeResource(uriInfo, request, id);
  }
  
} 