package nl.utwente.di.model;

import jakarta.annotation.Resource;
import jakarta.xml.bind.annotation.XmlRootElement;
import nl.utwente.di.dao.BikeDao;

@XmlRootElement
@Resource
public class Bike {
  private int id;
  private String ownerName;
  private String color;
  private String gender;

  private Bike(){}

  public Bike(String color, String gender) {
    this.id = BikeDao.instance.getModel() != null ? BikeDao.instance.getModel().size() + 1 : 1;
    this.color = color;
    this.gender = gender;
  }

  public Bike(String id, String ownerName, String color, String gender) {
    this.id = Integer.parseInt(id);
    this.ownerName = ownerName;
    this.color = color;
    this.gender = gender;
  }

  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }

  public String getOwnerName() {
    return ownerName;
  }
  public void setOwnerName(String ownerName) {
    this.ownerName = ownerName;
  }

  public String getColor() {
    return color;
  }
  public void setColor(String color) {
    this.color = color;
  }

  public String getGender() {
    return gender;
  }
  public void setGender(String gender) {
    this.gender = gender;
  }
  
} 